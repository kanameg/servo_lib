/*==========================================================*/
/*! 
  @file       servo_lib.h
  @brief      ATTiny2313用 サーボ制御ライブラリ
  @author     kaname.g@gmail.com
  @note
*/
/*
  動作タイミング
      _____________                                      __
 ____|             |____________________________________|
     1                                                  1
     0             0
     |<--------------    20000us(625)   --------------->|
           タイマ1 256分周 @8Mhz  CTC動作(OCRA1一致時)
     |<----------->|
       1500us(186)
       タイマ0 64分周 @8Mhz CTC動作(OCRA0一致時)
*/
/*==========================================================*/

#include "servo_lib.h"

/*!
  define定義
 */

#define SERVO_DDR       DDRB
#define SERVO_DDR_CTRL  DDB4

#define SERVO_PORT      PORTB
#define SERVO_PORT_CTRL PB4

#define TIMER0_CPU_MHZ   (F_CPU / 1000000)
#define TIMER0_PRESCALE  (64)
#define TIMER0_US_TO_COMPA(us) (((us * TIMER0_CPU_MHZ) / TIMER0_PRESCALE) - 1)


/*!
  グローバル変数
 */
uint16_t servo_pulse;   //<  Hパルス期間
uint16_t servo_cycle;   //<  Hパスル周期


/*!
  @breif      タイマ1の初期化
 */
void timer1_init(void)
{
    // レジスタ設定
    /*
      OC1Aピン未使用
      OC1Bピン未使用
      CTCモード OCR0Aとの比較クリア
      同期動作 8分周  (20000us @8Mhz)
      OCR1A値 19999固定
    */
    servo_cycle = 19999;        // 20ms固定
    
    OCR1A   = servo_cycle;
    TCCR1B |= _BV(WGM12);
}

/*!
  @breif      タイマ1の開始
*/
void timer1_on(void)
{
    // カウンタクリア
    TCNT1H = 0x00;
    TCNT1L = 0x00;
    
    // 比較A割り込みフラグクリア、割込許可
    TIFR   |= _BV(OCF1A);
    TIMSK  |= _BV(OCIE1A);
    TCCR1B |= _BV(CS11);        // 8分周でカウント開始
}

/*!
  @breif      タイマ1の停止
 */
void timer1_off(void)
{
    // カウント停止
    TCCR1B &= ~(_BV(CS12) | _BV(CS11) | _BV(CS10));
    
    // 比較A割込禁止、フラグクリア
    TIMSK &= ~_BV(OCIE1A);
    TIFR  |= _BV(OCF1A);
}



/*!
  @breif      タイマ0の初期化
 */
void timer0_init(void)
{
    // 変数初期化
    servo_pulse = TIMER0_US_TO_COMPA(1500);  // 1.5ms
    
    // レジスタ設定
    /*
      OC0A,OC0B未使用
      CTCモード OCR0Aとの比較クリア
      同期動作 64分周
    */
    TCCR0A = _BV(WGM01);
    OCR0A  = servo_pulse;
}

/*!
  @breif      タイマ0の開始
 */
void timer0_on(void)
{
    // カウンタクリア
    TCNT0 = 0x00;
    
    // 比較A割込フラグクリア,割込許可
    TIFR  |= _BV(OCF0A);
    TIMSK |= _BV(OCIE0A);

    // 64分周でカウント開始
    TCCR0B |= _BV(CS01)|_BV(CS00);
}

/*!
  @breif      タイマ0の停止
 */
void timer0_off(void)
{
    // カウント停止
    TCCR0B &= ~(_BV(CS01)|_BV(CS00));

    // 比較A割込禁止、フラグクリア
    TIMSK &= ~_BV(OCIE0A);
    TIFR  |= _BV(OCF0A);
}

/*!
  @breif      サーボ制御初期化
 */
void servo_init(void)
{
    // ピン初期化
    SERVO_DDR  |= _BV(SERVO_DDR_CTRL);   // 出力設定
    SERVO_PORT &= ~_BV(SERVO_PORT_CTRL); // 'L'出力

    // タイマ関連初期化
    timer1_init();
    timer0_init();

    timer1_on();   // タイマ1 割込ON
    //timer0_off();  // タイマ0 割込OFF
}

/*!
  @breif      サーボ制御値変更
              'H'パルス期間の幅を変更
 */
void servo_control(uint16_t pulse_us)
{
    // 直接レジスタに書かず次周期で設定する
    if (1000 <= pulse_us && pulse_us <= 2000) {
        servo_pulse = TIMER0_US_TO_COMPA(pulse_us);
    }
}


/*
  割込ハンドラ
 */

/*!
  @breif      パルス立ち上がり時割込(TIMER1溢れ時)
 */
ISR(TIMER1_COMPA_vect){
    SERVO_PORT |= _BV(SERVO_PORT_CTRL); // 'H'出力
    
    // 比較値(OCR0A)修正してタイマ0スタート
    OCR0A = servo_pulse;
    timer0_on();
}


/*!
  @breif      パルス立ち下がり時割込(TIMER0比較一致時)
 */
ISR(TIMER0_COMPA_vect){
    SERVO_PORT &= ~_BV(SERVO_DDR_CTRL);  // 'L'出力

    // タイマ0停止
    timer0_off();
}

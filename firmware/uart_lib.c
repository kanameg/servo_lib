/*! 
  @file       uart_lib.c
  @brief      ATTiny2313用 UARTライブラリ
  @author     kaname.g@gmail.com
  @note
*/

#include "uart_lib.h"

/*!
  ポート定義
 */
#define UART_DDR      DDRD
#define UART_DDR_RXD  DDD0
#define UART_DDR_TXD  DDD1


/*!
  UART用のリングバッファ
 */
#define RX_BUFFER_SIZE (8)            //!< 受信バッファサイズ
uint8_t rx_buffer[RX_BUFFER_SIZE];    //!< 受信バッファ
volatile uint8_t rx_wp, rx_rp;        //!< 受信バッファ(書込 読出 ポインタ)


#define TX_BUFFER_SIZE (8)            //!< 送信バッファサイズ
uint8_t tx_buffer[TX_BUFFER_SIZE];    //!< 送信バッファ
volatile uint8_t tx_wp, tx_rp;        //!< 送信バッファ(書込 読出 ポインタ)


/*!
  @brief      UART 初期化
  @param[in]  baud  ボーレートレジスタ設定値
*/
void uart_init(uint16_t baud)
{
    rx_wp = 0;
    rx_rp = 0;
    tx_wp = 0;
    tx_rp = 0;

    // ポート設定
    UART_DDR |= _BV(UART_DDR_TXD);
    UART_DDR &= ~_BV(UART_DDR_RXD);

    // レジスタ設定
    UBRRH = (baud >> 8) & 0xff;
    UBRRL = baud & 0xff;
    
    UCSRA = _BV(U2X);
    UCSRB = _BV(RXCIE) | _BV(TXCIE) | _BV(RXEN) | _BV(TXEN);
    UCSRC = _BV(UCSZ1) | _BV(UCSZ0);
}

/*!
  @brief      データを1バイト送信
  @param[in]  b  送信データ
*/
void uart_put_byte(const uint8_t b)
{
    while ((tx_wp + 1) % TX_BUFFER_SIZE == tx_rp)
        ; // 送信バッファに空きがなければ待つ
    
    tx_buffer[tx_wp++] = b;
    tx_wp %= TX_BUFFER_SIZE;
    // 送信できる状態であれば送信する
    if (UCSRA & _BV(UDRE)) {
        if (tx_wp != tx_rp) {
            TXB = tx_buffer[tx_rp++];
            tx_rp %= TX_BUFFER_SIZE;
        }
    }
}

/*!
  @brief      1文字送信
  @param[in]  c  送信文字
*/
void uart_putc(const char c)
{
    if (c == '\n') uart_put_byte('\r'); // "\n"  =>  "\r\n" に変換
    uart_put_byte((uint8_t)c);
}

/*!
  @brief      文字列を送信
  @param[in]  s 送信文字列ポインタ
*/
void uart_puts(const char *s)
{
    while(*s) {
        uart_putc(*s++);
    }
}

/*!
  @brief      データを1バイト受信
  @retval     受信文字
*/
uint8_t uart_get_byte(void)
{
    uint8_t b;
    
    while (rx_wp == rx_rp) ; // 受信するまでループ
    b = rx_buffer[rx_rp++];
    rx_rp %= RX_BUFFER_SIZE;
    
    return b;
}

/*!
  @brief      データを1バイト受信
  @retval     受信文字
*/
char uart_getc(void)
{
    char c;
    
    c = (char)uart_get_byte();
    c = (c == '\r') ? '\n' : c;  // 改行コード変換
    uart_putc(c);                // ローカルエコー
    
    return c;
}

/*!
  @brief      文字列受信
  @param[out] 受信文字列ポインタ
  @retval     文字列長
 */
uint8_t uart_gets(char *s)
{
    uint8_t n;
    char c;
    
    n = 0;
    do {
        c = uart_getc();
        if (c == '\n') {
            c = '\0'; // 改行を終端文字に変更
        }
        s[n++] = c;
    } while (c);
    
    return n - 1;  // 終端文字は含まない
}


/*!
  @brief       受信割り込み
*/
ISR(USART_RX_vect)
{
    rx_buffer[rx_wp++] = RXB;
    rx_wp %= RX_BUFFER_SIZE;
}

/*!
  @brief       送信完了割り込み
*/
ISR(USART_TX_vect)
{
    if (tx_wp != tx_rp) {
        TXB = tx_buffer[tx_rp++];
        tx_rp %= TX_BUFFER_SIZE;
    }
}

/*==========================================================*/
/*! 
  @file       servo_lib.h
  @brief      ATTINY25,45,85用 サーボ制御ライブラリ
  @author     kaname.g@gmail.com
  @note
*/
/*==========================================================*/
#ifndef SERVO_LIB_H
#define SERVo_LIB_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/boot.h>
#include <avr/wdt.h>
#include <inttypes.h>

uint16_t t0_int_count;
uint16_t t1_int_count;

/*!
  @breif      API 一覧
 */
void timer1_init(void);
void timer1_on(void);
void timer0_init(void);
void timer0_on(void);
void servo_init(void);
void servo_control(uint16_t pulse_us);

#endif /* SERVO_LIB_H */

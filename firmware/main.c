/* Name: main.c
 * Author: <insert your name here>
 * Copyright: <insert your copyright message here>
 * License: <insert your license reference here>
 */

#include <avr/io.h>
#include "servo_lib.h"
#include "uart_lib.h"

const char hex[] = {'0','1','2','3','4','5','6','7',
                    '8','9','A','B','C','D','E','F'};

/*!
  @breif      文字列を数値に変換
*/
uint16_t strtonum(char *str)
{
    uint16_t num = 0;
    char *p;
    
    p = str;
    while (*p != '\0' && '0' <= *p && *p <= '9') {
        num = num * 10 + (*p++ - '0');
    }

    return num;
}

/*!
  @breif      8バイト値 => 文字列変換
 */
void uint8tohex(uint8_t val, char *str)
{
    str[0] = hex[(val >> 4) & 0x0f];
    str[1] = hex[val & 0x0f];
    str[2] = '\0';
}

/*!
  @breif      16バイト値 => 文字列変換
 */
void uint16tohex(uint16_t val, char *str)
{
    str[0] = hex[(val >> 12) & 0x0f];
    str[1] = hex[(val >> 8) & 0x0f];
    str[2] = hex[(val >> 4) & 0x0f];
    str[3] = hex[val & 0x0f];
    str[4] = '\0';
}


int main(void)
{
    char cmd_str[16];
    char reg_str[16];
    uint16_t pulse = 1500;
    
    /* insert your hardware initialization here */
    uart_init(0x33); // UART 速度19200bps
    servo_init();    // サーボ制御の開始
    sei();           // 割込許可
    
    for(;;){
        /* insert your main loop code here */
        uart_gets(cmd_str);
        pulse = strtonum(cmd_str);
        if (pulse < 1000) {
            pulse = 1000;
        }
        else if (pulse > 2000) {
            pulse = 2000;
        }
        servo_control(pulse);
    }
    return 0;   /* never reached */
}
